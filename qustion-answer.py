import openai
from documents.document_list import document_list
from dotenv import load_dotenv
import os

load_dotenv()

openai.api_key = os.getenv('OPEN_API_KEY')

response = openai.Answer.create(
 search_model="ada",
 model="curie",
 question="What is the FFT?",
 documents=document_list,
 examples_context="The FFT Düsseldorf (Forum Freies Theater) has been in existence since 1999; since 2004 under the artistic direction of Kathrin Tiedemann. It is an international production house for free performing arts and operates in a network of production venues, theaters and other partners both regionally and internationally",
 examples=[["what is theater?","theater is a collaborative form of performing art that uses live performers, usually actors or actresses, to present the experience of a real or imagined event before a live audience in a specific place, often a stage."],["What is the FFT?", "The FFT Düsseldorf (Forum Freies Theater) has been in existence since 1999"]],
 max_tokens=1000,
 stop=["\n", "<|endoftext|>"],
)

print(response)

with open('responses.txt', 'a') as f:
    f.write(response)
    f.write('\n')