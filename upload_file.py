import openai
from dotenv import load_dotenv

load_dotenv()

openai_key = os.getenv('OPEN_API_KEY')
 
response = openai.File.create(
 file =open("sample.jsonl"),
 purpose='answers'
)
 
print(response)


# curl https://api.openai.com/v1/files \
#   -H "Authorization: Bearer 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" \
#   -F purpose="answers" \
#   -F file="sample.jsonl"