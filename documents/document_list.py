document_list = [
    "Theatre is the institution which commands the oldest repository of knowledge about the social power of acting. The theatre of the digital natives, as we observe and accompany it at the FFT Düsseldorf, represents a new game of shared knowledge, digital technologies, and spaces of responsibility."
]
